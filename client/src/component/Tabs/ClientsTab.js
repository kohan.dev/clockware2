import React, { useState, useEffect } from 'react';
import AddButton from './AddButton';
import ClientDataPanel from '../DataPanel/ClientDataPanel';
import {isEmail} from '../../helpers/validation';
import ClientsTable from './Tables/ClientsTable';
import DeleteDialog from './DeleteDialog';

function ClientsTab(props) {
    const [clientEdit, setClientEdit] = useState({
        name: '',
        email: '',
    });
    const [flag, setFlag] = useState({
        showPanel: false,
        addNew: true,
    });

    const [showDelDialog, setShowDelDialog] = useState(false);
    const [delId, setDelId] = useState('');

    /* eslint-disable */
    useEffect(() => {
        props.fetchClients();
    }, []);
    /* eslint-enable */

    const changeClientName = name => setClientEdit({ ...clientEdit, name: name });
    const changeClientEmail = email => setClientEdit({ ...clientEdit, email: email });
    const handleAddButton = () => setFlag({ showPanel: true, addNew: true });

    const handleClientSave = event => {
        event.preventDefault();
        if (clientEdit.name.length >= 3 && isEmail(clientEdit.email)) {
            if (flag.addNew) {
                props.addClient(clientEdit);
            } else {
                props.editClient(clientEdit);
            }
            setFlag({ ...flag, showPanel: false });
            setClientEdit({ name: '', email: '' });
        }
    };

    const handleClientCancel = event => {
        event.preventDefault();
        setFlag({ ...flag, showPanel: false });
        setClientEdit({ name: '', email: '' });
    };

    const clickEdit = id => {
        props.clients.forEach((client) => {
            if (client.id === id) {
                setClientEdit(client);
            }
        });
        setFlag({ showPanel: true, addNew: false });
    };


    const clientDelete = () => props.deleteClient(delId);

    const clickDel = id => {
        setDelId(id);
        setShowDelDialog(true);
    };

    const deleteDialog = () => {
        if (showDelDialog) {
            return (
                <DeleteDialog
                    context={contextClient()}
                    setDelete={setShowDelDialog}
                    deleteEntry={clientDelete}
                />
            )
        } else {
            return null
        }
    };

    const contextClient = () => {
        let context = '';
        props.clients.forEach((client) => {
            if (client.id === delId) context = `id:${client.id} ${client.name}`
        });
        return context;
    };


    const clientsTablesArr = () => {
        if (props.clients[0]) {
            return props.clients.map((client) => {
                return ({
                    id: client.id,
                    name:client.name,
                    email: client.email,
                })
            })
        }
        return []
    };

    const showPanelOrList = () => {
        if (flag.showPanel) {
            return (
                <ClientDataPanel
                    clientEdit={clientEdit}
                    changeClientName={changeClientName}
                    changeClientEmail={changeClientEmail}
                    handleClientSave={handleClientSave}
                    handleClientCancel={handleClientCancel}
                    addNew={flag.addNew}
                />
            )
        } else {
            return (
                <React.Fragment>
                    <AddButton
                        nameAdd='клиента'
                        handleButton={handleAddButton}
                    />
                    <ClientsTable
                        listArr={clientsTablesArr()}
                        clickEdit={clickEdit}
                        clickDel={clickDel}
                    />
                </React.Fragment>
            )
        }
    };

    return (
        <React.Fragment>
            {showPanelOrList()}
            {deleteDialog()}
        </React.Fragment>
    );
}

export default ClientsTab;